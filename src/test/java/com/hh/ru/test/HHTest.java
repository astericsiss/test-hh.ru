package com.hh.ru.test;

import com.hh.ru.helper.TestHelp;
import com.hh.ru.helper.TestListener;
import io.qameta.allure.Description;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import com.hh.ru.data.model.LoginPositive;
import com.hh.ru.data.model.Resume;
import com.hh.ru.data.model.Vacancy;
import com.hh.ru.pages.HomePage;
import com.hh.ru.constants.ConstantPage.ConstantAccountPage;
import com.hh.ru.pages.blocks.LoginBlock;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(TestListener.class)
public class HHTest extends TestHelp {

    @ParameterizedTest
    @Description(value = "Positive test, checks authorization")
    @CsvSource({"astericss@mail.ru, 365488"})
    void loginWithPasswordTest(String mail, String catchword) {
        new HomePage(driver)
                .open()
                .login()
                .comeFromPersonalAccount(mail, catchword);
        Assertions.assertNotEquals("https://hh.ru/account/login?backurl=%2F", driver.getCurrentUrl());
        driver.findElement(ConstantAccountPage.PROFILE_BUTTON).click();
        String nameResult = (String) js.executeScript("return arguments[0].innerText",
                driver.findElement(By.xpath("//div[@class='supernova-dropdown']//div[@class='supernova-dropdown-section'][1]")));
        Assertions.assertEquals("Александр A", nameResult);
    }

    @ParameterizedTest
    @Description(value = "Negative test, checks authorization")
    @CsvFileSource(resources = "/loginNeg.csv")
    void loginWithPasswordNegativeTest(String mail, String catchword) {
        driver.manage().window().setSize(new Dimension(800, 800));
        LoginBlock edit = new LoginBlock(driver);
        PageFactory.initElements(driver, edit);
        edit.open().login();
        edit.setLoginWithPassword()
                .setMail(mail)
                .setPassword(catchword)
                .save();
        List<WebElement> textResult = driver
                .findElements(By.xpath("//div[@class='account-login-tile-content']//div[@class='bloko-form-row']//div[@data-qa='account-login-error']"));
        textResult.forEach(s -> assertTrue(s.getText().equals("Неправильные данные для входа. Пожалуйста, попробуйте снова.") ||
                s.getText().equals("Пожалуйста, подтвердите, что вы не робот")));
    }

    @Test
    @Description(value = "Test checks job search")
    void jobSearchTest() throws IOException{
        Vacancy vacancy = getTestDataNow("Developer", Vacancy.class);
        new HomePage(driver).open()
                .clickOnFilter()
                .jobSearch(vacancy);
        int countVacancies = driver.findElements(By.xpath("//div[@data-qa='vacancy-serp__results']//div[@class='vacancy-serp-item']")).size();
        Assertions.assertEquals(20, countVacancies);
        List<WebElement> textResult = driver.findElements(By.xpath("//div[@data-qa='vacancy-serp__results']"));
        textResult.forEach(s -> assertTrue(s.getText().contains("Разработчик") &
                s.getText().contains("Developer")));
    }

    @Test
    @Description(value = "Test checks for filling out a resume")
    void resumeTest() throws IOException {
        LoginPositive loginPositive = getTestDataNow("LoginPositive", LoginPositive.class);
        Resume resume = getTestDataNow("Tester",Resume.class);
        new HomePage(driver)
                .open()
                .login()
                .comeFromPersonalAccount(loginPositive.getMail(), loginPositive.getCatchword())
                .clickToConstructSummary()
                .setContactName(resume)
                .setContactSurname(resume)
                .setCurrentCity(resume)
                .setBirthday(resume)
                .setBirthMonth(resume)
                .setBirthYear(resume)
                .setGender(resume)
                .deleteCitizenship()
                .setCitizenship(resume)
                .setExperience(resume)
                .setIncome(resume)
                .setCareerObjective(resume)
                .setCurrency(resume)
                .setWorkExperience(resume)
                .setPersonalInformation(resume)
                .setKeySkills(resume)
                .setEducation(resume)
                .setRemoval(resume)
                .setEmployment(resume)
                .setWorkSchedule(resume)
                .setHasVehicle()
                .saveResume();
        String text = (String) js.executeScript("return arguments[0].innerText",
                driver.findElement(By.xpath("//div[@class='suitablevacancies__published']//span[contains(text(),'Резюме успешно опубликовано')]")));
        Assertions.assertTrue(text.contains("Резюме успешно опубликовано."));

    }
}