package com.hh.ru.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Vacancy {

    private String keyword;
    private String searchOnly;
    private String specialization;
    private String industry;
    private String city;
    private String incomeLevel;
    private String currency;
    private String experience;
    private String employmentType;
    private String schedule;
    private String searchResultSettings;
    private String sorting;
    private String outputFilter;
    private String showOnPage;

}


