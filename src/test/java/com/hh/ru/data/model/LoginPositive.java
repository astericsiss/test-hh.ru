package com.hh.ru.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoginPositive {

    private String mail;
    private String catchword;

}
