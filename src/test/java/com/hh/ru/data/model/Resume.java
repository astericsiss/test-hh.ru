package com.hh.ru.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Resume {

    private String contactName;
    private String contactSurname;
    private String currentCity;
    private String birthday;
    private String birthMonth;
    private String birthYear;
    private String gender;
    private String citizenship;
    private String experience;
    private String careerObjective;
    private String income;
    private String currency;
    private String startOfMonth;
    private String startOfYear;
    private String byTime;
    private String endOfMonth;
    private String endOfYear;
    private String organization;
    private String position;
    private String responsibility;
    private String personalInformation;
    private String keySkills;
    private String primaryEducation;
    private String faculty;
    private String speciality;
    private String graduationYear;
    private String education;
    private String removal;
    private String removalCity;
    private String employment;
    private String workSchedule;

}
