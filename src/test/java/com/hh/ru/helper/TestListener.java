package com.hh.ru.helper;

import io.qameta.allure.Attachment;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import com.hh.ru.test.HHTest;

import java.util.Optional;


public class TestListener implements TestWatcher {

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        TestWatcher.super.testDisabled(context, reason);
        Object testClass = context.getRequiredTestInstance();
        WebDriver driver = ((HHTest) testClass).driver;
        driver.close();
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        TestWatcher.super.testSuccessful(context);
        Object testClass = context.getRequiredTestInstance();
        WebDriver driver = ((HHTest) testClass).driver;
        if (driver != null) {
            saveAllureScreenshot(driver);
            driver.close();
        }
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        TestWatcher.super.testAborted(context, cause);
        Object testClass = context.getRequiredTestInstance();
        WebDriver driver = ((HHTest) testClass).driver;
        driver.close();
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        Object testClass = context.getRequiredTestInstance();
        WebDriver driver = ((HHTest) testClass).driver;
        if (driver != null) {
            saveAllureScreenshot(driver);
            driver.close();
        }
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    protected byte[] saveAllureScreenshot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

}
