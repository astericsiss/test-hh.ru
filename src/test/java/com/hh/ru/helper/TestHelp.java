package com.hh.ru.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import static com.hh.ru.config.Config.IMPLICITLY_WAITE;
import static com.hh.ru.config.Config.BROWSER;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestHelp {
    public WebDriver driver;
    public JavascriptExecutor js;
    public TakesScreenshot ts;
    private static final ObjectMapper mapper = new ObjectMapper();


    @BeforeEach
    public void setProperty() {
        buildDriver();
    }

    private void buildDriver() {
        switch (BROWSER) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            default:
                System.out.println("Не корректный браузер: " + BROWSER);
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(IMPLICITLY_WAITE));
        js = (JavascriptExecutor) driver;
        ts = (TakesScreenshot) driver;
    }
    public static <T> T getTestDataNow(String dataName, Class<T> className) throws IOException {
        return mapper.readValue(new File("src/test/java/com/hh/ru/data/" + dataName + ".json"), className);
    }
}
