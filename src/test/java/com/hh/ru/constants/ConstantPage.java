package com.hh.ru.constants;

import org.openqa.selenium.By;

public class ConstantPage {

    public static class ConstantJobFilterPage {
        public static final By KEYWORDS = By.xpath("//input[@id='advancedsearchmainfield']");
        public static final By SEARCH_ONLY_CHECKBOX = By.xpath("//div[contains(text(),'Искать только')]/../following-sibling::div[@class='bloko-form-item-baseline']//span//parent::label");
        public static final By SPECIALIZATION_BUTTON = By.xpath("//button[@data-qa='resumesearch__profroles-switcher']");
        public static final By SPECIALIZATION_INPUT = By.xpath("//div[@class='bloko-modal-container bloko-modal-container_visible']//input[@class='bloko-input']");
        public static final By SPECIALIZATION_INPUT_CLICK = By.xpath("//div[@class='bloko-modal-overlay bloko-modal-overlay_visible']//div[@class='bloko-tree-selector-popup-content']");
        public static final By SPECIALIZATION_SAVE = By.xpath("//button[@class='bloko-button bloko-button_kind-primary']//span[text()='Выбрать']");
        public static final By INDUSTRY_BUTTON = By.xpath("//button[@data-qa='industry-addFromList']");
        public static final By INDUSTRY_INPUT = By.xpath("//div[@class='bloko-modal-container bloko-modal-container_visible']//input[@class='bloko-input Bloko-TreeSelectorPopup-Search']");
        public static final By INDUSTRY_INPUT_CLICK = By.xpath("//div[@class='bloko-modal-container bloko-modal-container_visible']//div[@class='bloko-tree-selector-popup-content Bloko-TreeSelectorPopup']//label");
        public static final By INDUSTRY_SAVE = By.xpath("//div[contains(@class,'bloko-modal-container')]//span[@class='bloko-button__content']");
        public static final By CITY_DELETE = By.xpath("//span[@data-qa='bloko-tag__cross']");
        public static final By CITY_BUTTON = By.xpath("//button[@data-qa='resumes-search-region-selectFromList']");
        public static final By CITY_INPUT = By.xpath("//div[@data-qa='bloko-modal']//input[@class='bloko-input Bloko-TreeSelectorPopup-Search']");
        public static final By CITY_INPUT_CLICK = By.xpath("//div[@class='bloko-modal-container bloko-modal-container_visible']//div[@class='bloko-tree-selector-popup-content Bloko-TreeSelectorPopup']//label[@class='bloko-checkbox']");
        public static final By CITY_SAVE = By.xpath("//div[contains(@class,'bloko-modal-container')]//span[@class='bloko-button__content']");
        public static final By INCOME_INPUT = By.xpath("//input[@data-qa = 'vacancysearch__compensation-input']");
        public static final By FIXED_INCOME_LEVEL_BUTTON = By.xpath("//span[contains(text(),'Показывать только вакансии')]//parent::label");
        public static final By CURRENCY_INPUT= By.xpath("//select[@data-qa='vacancysearch__compensation-currency']");
        public static final By EXPERIENCE_RADIO_BUTTON= By.xpath("//div[contains(text(),'Требуемый опыт работы')]/../../following-sibling::div//div[@class='bloko-form-item-baseline']//span//parent::label");
        public static final By EMPLOYMENT_CHECKBOX = By.xpath("//div[contains(text(),'Тип занятости')]/../../following-sibling::div//div[@class='bloko-form-item-baseline']//span//parent::label");
        public static final By SCHEDULE_CHECKBOX = By.xpath("//div[contains(text(),'График работы')]/../../following-sibling::div//div[@class='bloko-form-item-baseline']//span//parent::label");
        public static final By SETTINGS_RESULT_SEARCH_CHECKBOX = By.xpath("//div[contains(text(),'Настройки результатов поиска')]/../../following-sibling::div//div[@class='bloko-form-item-baseline']//span//parent::label");
        public static final By SORTING_RADIO_BUTTON = By.xpath("//div[contains(text(),'Сортировка')]/../../following-sibling::div//div[@class='bloko-form-item-baseline']//span//parent::label");
        public static final By FILTER_OUTPUT_RADIO_BUTTON = By.xpath("//div[contains(text(),'Выводить')]/../../following-sibling::div//div[@class='bloko-form-item-baseline']//span//parent::label");
        public static final By SHOW_ON_PAGE_RADIO_BUTTON = By.xpath("//div[contains(text(),'Показывать на странице')]/../../following-sibling::div//div[@class='bloko-form-item-baseline']//span//parent::label");
        public static final By SUBMIT= By.xpath("//input[@data-qa='vacancysearch__submit']");
    }
    public static class ConstantLoginPage {
        public static final By LOGIN_WITH_PASSWORD = By.xpath("//button[contains(text(),'Войти')]");
        public static final By MAIL_INPUT = By.xpath("//div[@class='bloko-form-item']//input[@name='username']");
        public static final By PASSWORD_INPUT = By.xpath("//input[@aria-label='Введите пароль']");
        public static final By ENTER = By.xpath("//span[text()='Войти']");
    }
    public static class ConstantAccountPage {
        public static final By PROFILE_BUTTON = By.xpath("//button[contains(@data-qa,'mainmenu_applicantProfile')]");
    }
    public static class ConstantHomePage {
        public static final By LOGIN_BUTTON = By.xpath("//a[contains(text(),'Войти')]");
        public static final By FILTER_BUTTON = By.xpath("//span[contains(@class,'bloko-icon_filters-24')]");
    }

}
