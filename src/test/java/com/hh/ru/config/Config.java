package com.hh.ru.config;

public class Config {
    public static final String BROWSER = "chrome";
    public static final int IMPLICITLY_WAITE = 10;
    public static final int EXPLICITLY_WAITE = 20;
}
