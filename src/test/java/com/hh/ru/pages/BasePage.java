package com.hh.ru.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static com.hh.ru.config.Config.EXPLICITLY_WAITE;

public abstract class BasePage {
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }
    public HomePage open() {
        driver.get("https://hh.ru");
        return new HomePage(driver);
    }


    public WebElement wailElementIsVisible(WebElement webElement) {
        new WebDriverWait(driver, Duration.ofSeconds(EXPLICITLY_WAITE)).until(ExpectedConditions.visibilityOf(webElement));
        return webElement;
    }
    public WebElement getElementByXpath(By xpath){
        return driver.findElement(xpath);
    }
    public List<WebElement> getElementsByXpath(By xpath){
        return driver.findElements(xpath);
    }
    public WebElement getWebElementList(By xpath, String element) {
        return driver.findElements(xpath).stream()
                .filter(s -> s.getText().equalsIgnoreCase(element))
                .findAny()
                .get();
    }
    public WebElement getWebElement(By xpath, String element) {
        WebElement webElement = getElementByXpath(xpath);
        return webElement.findElement(By.xpath("//*[contains(text(),'" + element + "')]"));
    }
}
