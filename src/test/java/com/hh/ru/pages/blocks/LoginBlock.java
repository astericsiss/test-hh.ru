package com.hh.ru.pages.blocks;

import com.hh.ru.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginBlock extends BasePage {

    @FindBy(xpath = "//button[contains(text(),'Войти')]")
    WebElement loginWithPassword;

    @FindBy(xpath = "//div[@class='bloko-form-item']//input[@name='username']")
    WebElement mailInput;

    @FindBy(xpath = "//input[@aria-label='Введите пароль']")
    WebElement passwordInput;

    @FindBy(xpath = "//span[text()='Войти']")
    WebElement enter;


    public LoginBlock(WebDriver driver) {
        super(driver);
    }

    public LoginBlock setLoginWithPassword() {
        loginWithPassword.click();
        return this;
    }

    public LoginBlock setMail(String value) {
        mailInput.sendKeys(value);
        return this;
    }

    public LoginBlock setPassword(String value) {
        passwordInput.sendKeys(value);
        return this;
    }

    public LoginBlock save() {
        enter.click();
        return this;
    }

}
