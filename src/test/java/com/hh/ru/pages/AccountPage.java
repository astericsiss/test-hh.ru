package com.hh.ru.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class AccountPage extends BasePage {


    @FindBy(xpath = "//div[@class='bloko-columns-wrapper']//a[@data-qa='mainmenu_createResume']")
    public WebElement constructSummaryClick;

    public AccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ResumePage clickToConstructSummary() {
        wailElementIsVisible(constructSummaryClick).click();
        return new ResumePage(driver);
    }

}
