package com.hh.ru.pages;

import com.hh.ru.constants.ConstantPage;
import com.hh.ru.data.model.Vacancy;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;

public class JobFilterPage extends BasePage {

    public JobFilterPage(WebDriver driver) {
        super(driver);
    }

    public JobFilterPage jobSearch(Vacancy vacancy) {
        getElementByXpath(ConstantPage.ConstantJobFilterPage.KEYWORDS).sendKeys(vacancy.getKeyword());

        getWebElementList(ConstantPage.ConstantJobFilterPage.SEARCH_ONLY_CHECKBOX,vacancy.getSearchOnly());

        getElementByXpath(ConstantPage.ConstantJobFilterPage.SPECIALIZATION_BUTTON).click();
        getElementByXpath(ConstantPage.ConstantJobFilterPage.SPECIALIZATION_INPUT).sendKeys(vacancy.getSpecialization());
        getWebElement(ConstantPage.ConstantJobFilterPage.SPECIALIZATION_INPUT_CLICK, vacancy.getSpecialization()).click();
        getElementByXpath(ConstantPage.ConstantJobFilterPage.SPECIALIZATION_SAVE).click();

        getElementByXpath(ConstantPage.ConstantJobFilterPage.INDUSTRY_BUTTON).click();
        getElementByXpath(ConstantPage.ConstantJobFilterPage.INDUSTRY_INPUT).sendKeys(vacancy.getIndustry());
        getWebElement(ConstantPage.ConstantJobFilterPage.INDUSTRY_INPUT_CLICK,vacancy.getIndustry()).click();
        getElementByXpath(ConstantPage.ConstantJobFilterPage.INDUSTRY_SAVE).click();

        getElementByXpath(ConstantPage.ConstantJobFilterPage.CITY_DELETE).click();

        getElementByXpath(ConstantPage.ConstantJobFilterPage.CITY_BUTTON).click();
        getElementByXpath(ConstantPage.ConstantJobFilterPage.CITY_INPUT).sendKeys(vacancy.getCity());
        getWebElement(ConstantPage.ConstantJobFilterPage.CITY_INPUT_CLICK,vacancy.getCity()).click();

        getElementByXpath(ConstantPage.ConstantJobFilterPage.CITY_SAVE).click();

        getElementByXpath(ConstantPage.ConstantJobFilterPage.INCOME_INPUT).sendKeys(vacancy.getIncomeLevel());

        WebElement fixedIncomeLevelResult = getElementByXpath(ConstantPage.ConstantJobFilterPage.FIXED_INCOME_LEVEL_BUTTON);
        fixedIncomeLevelResult.sendKeys(Keys.SPACE);

        Select dropdown = new Select(getElementByXpath(ConstantPage.ConstantJobFilterPage.CURRENCY_INPUT));
        dropdown.selectByValue(vacancy.getCurrency());

        getWebElementList(ConstantPage.ConstantJobFilterPage.EXPERIENCE_RADIO_BUTTON, vacancy.getExperience()).sendKeys(Keys.SPACE);

        getWebElementList(ConstantPage.ConstantJobFilterPage.EMPLOYMENT_CHECKBOX, vacancy.getEmploymentType()).sendKeys(Keys.SPACE);

        getWebElementList(ConstantPage.ConstantJobFilterPage.SCHEDULE_CHECKBOX, vacancy.getSchedule()).sendKeys(Keys.SPACE);

        getWebElementList(ConstantPage.ConstantJobFilterPage.SETTINGS_RESULT_SEARCH_CHECKBOX, vacancy.getSearchResultSettings()).sendKeys(Keys.SPACE);

        getWebElementList(ConstantPage.ConstantJobFilterPage.SORTING_RADIO_BUTTON, vacancy.getSorting()).sendKeys(Keys.SPACE);

        getWebElementList(ConstantPage.ConstantJobFilterPage.FILTER_OUTPUT_RADIO_BUTTON, vacancy.getOutputFilter()).sendKeys(Keys.SPACE);

        getWebElementList(ConstantPage.ConstantJobFilterPage.SHOW_ON_PAGE_RADIO_BUTTON, vacancy.getShowOnPage()).sendKeys(Keys.SPACE);

        getElementByXpath(ConstantPage.ConstantJobFilterPage.SUBMIT).click();
        return this;
    }
}
