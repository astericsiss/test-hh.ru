package com.hh.ru.pages;


import com.hh.ru.constants.ConstantPage;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public AccountPage comeFromPersonalAccount(String mail,String catchword){
        getElementByXpath(ConstantPage.ConstantLoginPage.LOGIN_WITH_PASSWORD).click();
        getElementByXpath(ConstantPage.ConstantLoginPage.MAIL_INPUT).sendKeys(mail);
        getElementByXpath(ConstantPage.ConstantLoginPage.PASSWORD_INPUT).sendKeys(catchword);
        getElementByXpath(ConstantPage.ConstantLoginPage.ENTER).click();
        return new AccountPage(driver);
    }
}
