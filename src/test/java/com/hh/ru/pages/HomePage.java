package com.hh.ru.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.hh.ru.constants.ConstantPage.ConstantHomePage;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public HomePage open() {
        return super.open();
    }

    public LoginPage login() {
        wailElementIsVisible(getElementsByXpath(ConstantHomePage.LOGIN_BUTTON)
                .stream()
                .filter(WebElement::isDisplayed)
                .findAny()
                .get())
                .click();
        return new LoginPage(driver);
    }

    public JobFilterPage clickOnFilter() {
        getElementByXpath(ConstantHomePage.FILTER_BUTTON).click();
        return new JobFilterPage(driver);
    }
}

