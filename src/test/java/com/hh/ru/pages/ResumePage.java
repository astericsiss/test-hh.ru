package com.hh.ru.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.hh.ru.data.model.Resume;

import java.util.Random;

public class ResumePage extends BasePage {

    @FindBy(xpath = "//input[@name = 'firstName[0].string']")
    public WebElement contactName;

    @FindBy(xpath = "//input[@name = 'lastName[0].string']")
    public WebElement contactSurname;

    @FindBy(xpath = "//button[@data-qa='resume-city-select']")
    public WebElement cityButton;

    @FindBy(xpath = "//input[@placeholder='Быстрый поиск']")
    public WebElement cityInput;

    @FindBy(xpath = "//div[@class ='bloko-tree-selector-popup']//div[@class ='bloko-tree-selector-item']")
    public WebElement cityInputClick;

    @FindBy(xpath = "//div[@class='bloko-modal-footer']//span[contains(text(),'Выбрать')]")
    public WebElement citySave;

    @FindBy(xpath = "//input[@placeholder='День']")
    public WebElement birthdayInput;

    @FindBy(xpath = "//select[@data-qa='resume__birthday__month-select']")
    public WebElement birthMonthInput;

    @FindBy(xpath = "//input[@placeholder='Год']")
    public WebElement birthYearInput;

    @FindBy(xpath = "//div[contains(text(),'Пол')]//..//..//div[@class='bloko-form-item-baseline']//label")
    public WebElement genderInput;

    @FindBy(xpath = "//div[@class='bloko-form-row']//button[@class='bloko-tag-button']")
    public WebElement citizenshipDelete;

    @FindBy(xpath = "//button[@data-qa='resume-citizenship-select']")
    public WebElement citizenshipButton;

    @FindBy(xpath = "//div[@class='bloko-tree-selector-popup-search']//input[@class='bloko-input']")
    public WebElement citizenshipInput;

    @FindBy(xpath = "//div[@class ='bloko-tree-selector-popup']//div[@class ='bloko-tree-selector-popup-content']")
    public WebElement citizenshipClick;

    @FindBy(xpath = "//div[@class='bloko-modal-footer']//span[contains(text(),'Выбрать')]")
    public WebElement citizenshipInputSave;

    @FindBy(xpath = "//div[contains(text(),'Опыт работы')]//..//..//div[@class='bloko-form-item-baseline']//label")
    public WebElement experienceInput;

    @FindBy(xpath = "//input[@placeholder='Желаемая должность']")
    public WebElement careerObjectiveInput;

    @FindBy(xpath = "//input[@placeholder='Зарплата на руки']")
    public WebElement incomeInput;

    @FindBy(xpath = "//select[@class='bloko-select bloko-select_flexible']")
    public WebElement currencyInput;

    @FindBy(xpath = "//button[@data-qa='resume-add-item']")
    public WebElement placeOfWorkButtonClick;

    @FindBy(xpath = "//div[@class='bloko-control-group__content-sized']//select[@data-name='experience[0].startDate.month']")
    public WebElement startOfMonthInput;

    @FindBy(xpath = "//input[@data-name='experience[0].startDate.year']")
    public WebElement startOfYearInput;

    @FindBy(xpath = "//label[@data-qa='resume-experience-tillnow resume-experience-tillnow_checked']")
    public WebElement endOfWorkCheckbox;

    @FindBy(xpath = "//select[@data-name='experience[0].endDate.month']")
    public WebElement endOfMonthInput;

    @FindBy(xpath = "//input[@data-name='experience[0].endDate.year']")
    public WebElement endOfYearInput;

    @FindBy(xpath = "//input[@placeholder='Название компании']")
    public WebElement organizationInput;

    @FindBy(xpath = "//input[@placeholder='Должность']")
    public WebElement positionInput;

    @FindBy(xpath = "//div[@class='bloko-form-item']//textarea[@name='experience[0].description']")
    public WebElement responsibilityInput;

    @FindBy(xpath = "//button[@data-qa='experience-modal-save']")
    public WebElement placeOfWorkSave;

    @FindBy(xpath = "//span[@class='bloko-textarea-wrapper']//textarea[@name='skills[0].string']")
    public WebElement personalInformationInput;

    @FindBy(xpath = "//input[@name='keySkills']")
    public WebElement keySkillsInput;

    @FindBy(xpath = "//select[@name='educationLevel[0].string']")
    public WebElement educationInput;

    @FindBy(xpath = "//div[@class='bloko-form-item-baseline']//button[@data-qa='resume-add-item']")
    public WebElement primaryEducationIClick;

    @FindBy(xpath = "//div[@class='resume-remove-item-gap']//input[@name='primaryEducation[0].name']")
    public WebElement primaryEducationInput;

    @FindBy(xpath = "//input[@name='primaryEducation[0].organization']")
    public WebElement facultyInput;

    @FindBy(xpath = "//input[@name='primaryEducation[0].result']")
    public WebElement specialityInput;

    @FindBy(xpath = "//input[@name='primaryEducation[0].year']")
    public WebElement graduationYearInput;

    @FindBy(xpath = "//button[contains(text(),'Переезд')]")
    public WebElement removalClick;

    @FindBy(xpath = "//div[contains(text(),'Переезд')]//..//..//div[@class='bloko-form-item-baseline']//label")
    public WebElement removalInput;

    @FindBy(xpath = "//button[@data-qa='resume-relocation-select']")
    public WebElement removalCityClick;

    @FindBy(xpath = "//input[@data-qa='bloko-tree-selector-popup-search']")
    public WebElement removalCityInput;

    @FindBy(xpath = "//div[@class='bloko-tree-selector-popup-content']")
    public WebElement removalCityInputClick;

    @FindBy(xpath = "//button[@class='bloko-button bloko-button_kind-primary']")
    public WebElement removalCitySave;

    @FindBy(xpath = "//button[contains(text(),'Занятость')]")
    public WebElement employmentClick;

    @FindBy(xpath = "//div[contains(text(),'Занятость')]//..//..//div[@class='bloko-form-item-baseline']//label")
    public WebElement employmentCheckbox;

    @FindBy(xpath = "//button[contains(text(),'График работы')]")
    public WebElement workScheduleClick;

    @FindBy(xpath = "//div[contains(text(),'График работы')]//..//..//div[@class='bloko-form-item-baseline']//label")
    public WebElement workScheduleCheckbox;

    @FindBy(xpath = "//button[contains(text(),'Наличие автомобиля')]")
    public WebElement hasVehicleClick;

    @FindBy(xpath = "//input[@name='hasVehicle[0].string']//parent::label")
    public WebElement hasVehicleCheckbox;

    @FindBy(xpath = "//button[@data-qa='resume-submit']")
    public WebElement resumeSave;


    public ResumePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ResumePage setContactName(Resume resume) {
        WebElement webElement = wailElementIsVisible(contactName);
        webElement.clear();
        webElement.sendKeys(resume.getContactName());
        return this;
    }

    public ResumePage setContactSurname(Resume resume) {
        WebElement webElement = wailElementIsVisible(contactSurname);
        webElement.clear();
        webElement.sendKeys(resume.getContactSurname());
        return this;
    }

    public ResumePage setCurrentCity(Resume resume) {
        wailElementIsVisible(cityButton).click();
        wailElementIsVisible(cityInput).sendKeys(resume.getCurrentCity());
        WebElement elementIndustry = cityInputClick;
        WebElement elementClickIndustry = elementIndustry
                .findElement(By.xpath("//*[contains(text(),'" + resume.getCurrentCity() + "')]"));
        elementClickIndustry.click();
        citySave.click();
        return this;
    }

    public ResumePage setBirthday(Resume resume) {
        birthdayInput.sendKeys(resume.getBirthday());
        return this;
    }

    public ResumePage setBirthMonth(Resume resume) {
        Select dropdown = new Select(birthMonthInput);
        dropdown.selectByVisibleText(resume.getBirthMonth());
        return this;
    }

    public ResumePage setBirthYear(Resume resume) {
        birthYearInput.sendKeys(resume.getBirthYear());
        return this;
    }

    public ResumePage setGender(Resume resume) {
        WebElement webElement = genderInput.findElement(By.xpath("//*[contains(text(),'" + resume.getGender() + "')]"));
        if (!webElement.isSelected()) {
            webElement.click();
        }
        return this;
    }

    public ResumePage deleteCitizenship() {
        citizenshipDelete.click();
        return this;
    }

    public ResumePage setCitizenship(Resume resume) {
        wailElementIsVisible(citizenshipButton).click();
        wailElementIsVisible(citizenshipInput).sendKeys(resume.getCitizenship());
        WebElement webElement = citizenshipClick
                .findElement(By.xpath("//*[contains(text(),'" + resume.getCitizenship() + "')]"));
        webElement.click();
        citizenshipInputSave.click();
        return this;
    }

    public ResumePage setExperience(Resume resume) {
        WebElement webElement = experienceInput
                .findElement(By.xpath("//*[contains(text(),'" + resume.getExperience() + "')]"));
        if (!webElement.isSelected()) {
            webElement.click();
        }
        return this;
    }

    public ResumePage setCareerObjective(Resume resume) {
        Random random = new Random();
        careerObjectiveInput.clear();
        careerObjectiveInput.sendKeys(resume.getCareerObjective()+random.nextInt(1000));
        wailElementIsVisible(careerObjectiveInput).sendKeys(Keys.ENTER);
        return this;
    }

    public ResumePage setIncome(Resume resume) {
        incomeInput.sendKeys(resume.getIncome());
        return this;
    }

    public ResumePage setCurrency(Resume resume) {
        Select dropdown = new Select(currencyInput);
        dropdown.selectByVisibleText(resume.getCurrency());
        return this;
    }

    public ResumePage setWorkExperience(Resume resume) {
        placeOfWorkButtonClick.click();
        Select dropdown = new Select(startOfMonthInput);
        dropdown.selectByValue(resume.getStartOfMonth());
        startOfYearInput.sendKeys(resume.getStartOfYear());
        boolean result = resume.getByTime().contains("По настоящее время");
        if (!result) {
            endOfWorkCheckbox.click();
            new Select(endOfMonthInput).selectByVisibleText(resume.getEndOfMonth());
            endOfYearInput.sendKeys(resume.getEndOfYear());
        }
        organizationInput.sendKeys(resume.getOrganization());
        positionInput.sendKeys(resume.getPosition());
        wailElementIsVisible(responsibilityInput).sendKeys(resume.getResponsibility());
        placeOfWorkSave.click();
        return this;
    }

    public ResumePage setPersonalInformation(Resume resume) {
        personalInformationInput.sendKeys(resume.getPersonalInformation());
        return this;
    }

    public ResumePage setKeySkills(Resume resume) {
        keySkillsInput.sendKeys(resume.getKeySkills());
        return this;
    }

    public ResumePage setEducation(Resume resume) {
        new Select(educationInput).selectByVisibleText(resume.getEducation());
        boolean result = resume.getEducation().contains("Среднее");
        if (!result) {
            primaryEducationIClick.click();
            primaryEducationInput.sendKeys(resume.getPrimaryEducation());
            wailElementIsVisible(primaryEducationInput).sendKeys(Keys.ENTER);
            facultyInput.sendKeys(resume.getFaculty());
            specialityInput.sendKeys(resume.getSpeciality());
            wailElementIsVisible(specialityInput).sendKeys(Keys.ENTER);
            graduationYearInput.sendKeys(resume.getGraduationYear());
        }
        return this;
    }

    public ResumePage setRemoval(Resume resume) {
        removalClick.click();
        WebElement webElementOne = removalInput.findElement(By.xpath("//*[contains(text(),'" + resume.getRemoval() + "')]"));
        if (!webElementOne.isSelected()) {
            webElementOne.click();
            removalCityClick.click();
            removalCityInput.sendKeys(resume.getRemovalCity());
            WebElement webElementTwo = removalCityInputClick.findElement(By.xpath("//*[contains(text(),'" + resume.getRemovalCity() + "')]"));
            webElementTwo.click();
            removalCitySave.click();
        }
        return this;
    }

    public ResumePage setEmployment(Resume resume) {
        employmentClick.click();
        WebElement webElement = employmentCheckbox.findElement(By.xpath("//*[contains(text(),'" + resume.getEmployment() + "')]"));
        if (!webElement.isSelected()) {
            webElement.click();
        }
        return this;
    }

    public ResumePage setWorkSchedule(Resume resume) {
        workScheduleClick.click();
        WebElement webElement = workScheduleCheckbox.findElement(By.xpath("//*[contains(text(),'" + resume.getWorkSchedule() + "')]"));
        if (!webElement.isSelected()) {
            webElement.click();
        }
        return this;
    }

    public ResumePage setHasVehicle() {
        hasVehicleClick.click();
        hasVehicleCheckbox.click();
        return this;
    }

    public void saveResume() {
        resumeSave.click();
    }
}
